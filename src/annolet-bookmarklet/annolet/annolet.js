annoletContainer();
getJsondata();

function annoletContainer(){
    //appending a div(annolet container) to body element of a webpage.
    var body = document.getElementsByTagName('body')[0];
    container = document.createElement('div');
    container.id = 'annolet-container';
    body.appendChild(container);

    //appending a CSS stylesheet to head element of a webpage, which is used to stylize the annolet container.
    var linktag = document.createElement('link');
    linktag.rel = "stylesheet";
    linktag.type = "text/css";
    linktag.href = "https://gl.githack.com/renarration/information-accessibility/raw/develop/src/annolet-bookmarklet/annolet/css/annolet.css"; 
    document.getElementsByTagName('head')[0].appendChild(linktag);

    // injecting html code
     document.getElementById('annolet-container').innerHTML = "<h3 id='annolet-header'>Semantic Overlay</h3>"+
    "<ul id='annolet-tools-menu' >"+
        "<li id='custom-tags-menu-list'>"+
        "<br>"+
        "</li>"+
        
         "<li id='conversion-menu-list'>"+
            "<div style='position:relative;top:19px;display:inline-block;'>"+
               "<button id='conversion-btn' >conversion</button>"+"<br>"+
               "<select id='conversion-menu'>"+
                  "<option value='num'>N</option>"+
                  "<option value='date'>D</option>"+
               "</select>"+ 
            "</div>"+
         "</li>"+
    "</ul>";
}

function getJsondata()
{
    var url = "https://gl.githack.com/renarration/information-accessibility/raw/develop/src/annolet-bookmarklet/annolet/custom_tags.json"; 
    $.get(url, function(data, status) {
        json_data = data;
        createMenulist(json_data);
    });
}

function createMenulist(result){
    for (var i = 0; i < result.main_menu.length; i++) { 
        var menu =  "<div style='position:relative;top:19px;display:inline-block;'>"+
                "<button id='category-"+i+"' class='customtag-menu-btn'>"+result.main_menu[i].name+" "+"Tag it..!</button>"+"<br>"+
                "<select class='customtag-menu-"+i+"' id='"+json_data.main_menu[i].name+"'>"+
                "</select>"+
                "</div>";
        $('#custom-tags-menu-list').append(menu);
        tags = result.main_menu[i].sub_menu;
        for( property in tags){
            $('.customtag-menu-'+i).append('<option value="'+tags[property]+'">'+property+'</option>')
        }
    }
    addclickEvents()
}

function annotateTag(markup_category){
    var selected_tag = document.getElementById(markup_category).value;
    if(window.getSelection){
        var text_to_annotate = window.getSelection();
        createcustomTag(selected_tag, text_to_annotate)
    }
    else if (document.selection && document.selection.type != "Control") {
        var text_to_annotate = document.selection.createRange().text;
        createcustomTag(selected_tag, text_to_annotate)
    }      
}

function createcustomTag(cus_tag, str){
     var custom_tag = document.createElement(cus_tag);
     custom_tag.textContent = str;
     custom_tag.style.backgroundColor = "yellow";
     var range = str.getRangeAt(0);
     range.deleteContents();
     range.insertNode(custom_tag);      
}

function convertNumsys(){
    var selected_numsys = 'ar-EG';
    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if(all[i].tagName == "PD-PRICE"){
            var tag_text = all[i].innerHTML;
            var text_toformat =  tag_text.replace(/\,/g,"");
            if(isNaN(text_toformat) == false) {
            	var num_to_convert = parseFloat(text_toformat);
            	var converted_num = num_to_convert.toLocaleString(selected_numsys);
            	all[i].innerHTML=  "<span class='highlight' style='color:green'>"+converted_num+"</span>";
        	}
        	else if(isNaN(text_toformat) == true) {
            	console.log("*****Invalid Number to process*****");
        	}
        }
    }
}

function formatDate(){
    var selected_format = 'ar-EG';
    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if(all[i].tagName == "NUM-DATE"){
            var tag_text = all[i].innerHTML;
            var num_to_format = new Date(tag_text);
            var formated_date = num_to_format.toLocaleDateString(selected_format)
            if (formated_date == "Invalid Date"){	
         		console.log("*****Invalid*****");
            }
            else {
            	all[i].innerHTML=  "<span class='highlight' style='color:green'>"+formated_date+"</span>";
            }
        }
    }
}

function addclickEvents() {
    document.getElementById('category-0').addEventListener('click', function() {
        var category = json_data.main_menu[0].name;
        annotateTag(category);
    }, false);
    document.getElementById('category-1').addEventListener('click', function() {
        var category = json_data.main_menu[1].name;
        annotateTag(category);
    }, false);
    document.getElementById('category-2').addEventListener('click', function() {
        var category = json_data.main_menu[2].name;
        annotateTag(category);
    }, false);
    document.getElementById('conversion-btn').addEventListener('click', function() {
        var conversion_value = document.getElementById('conversion-menu').value;
        if(conversion_value == 'num'){
            convertNumsys()
        }
        else if(conversion_value == 'date'){
            formatDate()
        }
    }, false);
}      
      


